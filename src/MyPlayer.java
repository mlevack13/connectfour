import java.util.ArrayList;
import java.util.Random;

/**
 * University of San Diego
 * COMP 285: Spring 2015
 * Instructor: Gautam Wilkins
 *
 * Implement your Connect Four player class in this file.
 */
public class MyPlayer extends Player {

    Random rand;

    private class Move {
        int move;
        double value;

        Move(int move) {
            this.move = move;
            this.value = 0.0;
        }
    }

    public MyPlayer() {
        rand = new Random();
        return;
    }

    public void setPlayerNumber(int number) {
        this.playerNumber = number;
    }


    public int chooseMove(Board gameBoard) {

        long start = System.nanoTime();
        Move bestMove = search(gameBoard, 8, -100, 100, this.playerNumber);

        System.out.println(bestMove.value);

        long diff = System.nanoTime()-start;
        double elapsed = (double)diff/1e9;
        System.out.println("Elapsed Time: " + elapsed + " sec");
        return bestMove.move;
    }

    public Move search(Board gameBoard, int maxDepth, double alpha, double beta, int playerNumber) {
        Move bestMove = new Move(3);
        bestMove.value = Integer.MIN_VALUE;
        int validMove = 1;
        for (int column = 0; column < Board.BOARD_SIZE && bestMove.value < beta; column++){
            int i = 0; // integer for new move value using alpha-beta pruning
            if(validMove == 1) {
                i = (3 + (column / 2)) % 7; //choosing move that is valid on the board
                validMove = 0;
            }
            else {
                if(column == 5) {
                    i = 0;
                } else {
                    i = ((3 + column) % 7) - (column + 1) - (column / 2);
                }
                validMove = 1;
            }
            if (!gameBoard.isColumnOpen(i)) {
                continue;
            }
            Move thisMove = new Move(i);
            gameBoard.move(playerNumber, i);
            int gameStatus = gameBoard.checkIfGameOver(i);
            if (gameStatus >= 0) {

                if (gameStatus == 0) {
                    // Tie game
                    thisMove.value = 0.0;
                } else if (gameStatus == playerNumber) {
                    // Win
                    thisMove.value = 1.0;
                } else {
                    // Loss
                    thisMove.value = -1.0;
                }

            } else if (maxDepth >= 1) {
                Move responseMove = search(gameBoard, maxDepth - 1, -beta, -alpha, (playerNumber == 1 ? 2 : 1));
                thisMove.value = -responseMove.value;

            } else {
                thisMove.value = heuristic(gameBoard, playerNumber);
            }
            if (thisMove.value > bestMove.value) {
                bestMove = thisMove;
                alpha = Math.max(bestMove.value, alpha);
            }

            gameBoard.undoMove(i);
        }
        return bestMove;
    }

    public double heuristic(Board gameBoardB, int playerNumber) {
        // This should return a number between -1.0 and 1.0.
        //
        // If the board favors playerNumber then the return value should be close to 1.0
        // If the board favors playerNumber's opponent then the return value should be close to 1.0
        // If the board favors neither player then the return value should be close to 0.0

        int[][] gameBoard = gameBoardB.getBoard();
        //opponent's number
        int opponentNumber = 0;

        // if passed in player number is player 1, oppo is player2
        if (playerNumber == 1) {
            opponentNumber = 2;
        } else {
            opponentNumber = 1;
        }

        int myFourInRow = 0;
        int opponentWin = 0;

        int myRowScore = 0;
        int maxRowScore = 0;

        int opponentScore = 0;
        int opponentMaxRow = 0;

        //loop through every move
        for(int c = 0; c < 7; c++) {
            //loop to check each column for 4 in a row
            for(int q = 0; q < 4; q++) {
                if(gameBoard[c][q] == playerNumber) {
                    myRowScore++;
                    if(gameBoard[c][q + 1] == playerNumber) {
                        myRowScore++;
                        if(gameBoard[c][q + 2] == playerNumber) {
                            myRowScore++;
                            if(gameBoard[c][q + 3] == playerNumber) {
                                myRowScore++;
                                myFourInRow = 1;
                            } else if(gameBoard[c][q + 3] == opponentNumber){
                                myRowScore = 0;
                            }
                        } else if(gameBoard[c][q + 2] == 0){
                            if(gameBoard[c][q + 3] == playerNumber) {
                                myRowScore++;
                            } else if(gameBoard[c][q + 3] == opponentNumber){
                                myRowScore = 0;
                            }
                        } else {
                            myRowScore = 0;
                        }
                    } else if(gameBoard[c][q + 1] == 0) {
                        if(gameBoard[c][q + 2] == playerNumber) {
                            myRowScore++;
                            if(gameBoard[c][q + 3] == playerNumber) {
                                myRowScore++;
                            }
                            else if(gameBoard[c][q + 3] == opponentNumber) {
                                myRowScore = 0;
                            }
                        } else if(gameBoard[c][q + 2] == 0) {

                            if(gameBoard[c][q + 3] == playerNumber) {
                                myRowScore++;
                            }
                            else if(gameBoard[c][q + 3] == opponentNumber) {
                                myRowScore = 0;
                            }
                        }
                        else {
                            myRowScore = 0;
                        }
                    }
                    else {
                        myRowScore = 0;
                    }
                } else {
                    myRowScore = 0;
                }
                if(myRowScore > maxRowScore) {
                    maxRowScore = myRowScore;
                }
                myRowScore = 0;
                if(gameBoard[c][q] == opponentNumber) {
                    opponentScore++;
                    if(gameBoard[c][q + 1] == opponentNumber) {
                        opponentScore++;
                        if(gameBoard[c][q + 2] == opponentNumber) {
                            opponentScore++;
                            if(gameBoard[c][q + 3] == opponentNumber) {
                                opponentScore++;
                                opponentWin = -1; //opponent's optimal moves are the neg of myplayer's max
                            }
                            else if(gameBoard[c][q + 3] == playerNumber){
                                opponentScore = 0;
                            }
                        } else if(gameBoard[c][q + 2] == 0){
                            if(gameBoard[c][q + 3] == opponentNumber) {
                                opponentScore++;
                            } else if(gameBoard[c][q + 3] == playerNumber){
                                opponentScore = 0;
                            }
                        } else {
                            opponentScore = 0;
                        }
                    } else if(gameBoard[c][q + 1] == 0) {
                        if(gameBoard[c][q + 2] == opponentNumber) {
                            opponentScore++;
                            if(gameBoard[c][q + 3] == opponentNumber) {
                                opponentScore++;
                            } else if(gameBoard[c][q + 3] == playerNumber) {
                                opponentScore = 0;
                            }
                        } else if(gameBoard[c][q + 2] == 0) {
                            if(gameBoard[c][q + 3] == opponentNumber) {
                                opponentScore++;
                            } else if(gameBoard[c][q + 3] == playerNumber) {
                                opponentScore = 0;
                            }
                        } else {
                            opponentScore = 0;
                        }
                    } else {
                        opponentScore = 0;
                    }
                } else {
                    opponentScore = 0;
                }

                if(opponentScore > opponentMaxRow) {
                    opponentMaxRow = opponentScore;
                }
                opponentScore = 0;
            }
        }

        int diagonalUp = 0;
        int diagonalDown = 0;
        int maxDiagonal = 0;

        int opponentDiagonal = 0;
        int opponentDiagonalDown = 0;
        int opponentDiagScore = 0;

        for(int c = 0; c < 7; c++) {
            for(int q = 0; q < 4; q++) {
                if (gameBoard[c][q] == playerNumber) {
                    if (c == 3) {
                        if (gameBoard[c + 1][q + 1] == playerNumber) {
                            diagonalUp++;
                            if (gameBoard[c + 2][q + 2] == playerNumber) {
                                diagonalUp++;
                                if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagonalUp++;
                                    myFourInRow = 1;
                                } else if (gameBoard[c + 3][q + 3] == opponentNumber) {
                                    diagonalUp = 0;
                                }
                            } else if (gameBoard[c][q + 2] == 0) {

                                if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagonalUp++;
                                } else if (gameBoard[c + 3][q + 3] == opponentNumber) {
                                    diagonalUp = 0;
                                }
                            } else {
                                diagonalUp = 0;
                            }
                        } else if (gameBoard[c + 1][q + 1] == 0) {

                            if (gameBoard[c + 2][q + 2] == playerNumber) {
                                diagonalUp++;

                                if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagonalUp++;
                                } else if (gameBoard[c + 3][q + 3] == opponentNumber) {
                                    diagonalUp = 0;
                                }
                            } else if (gameBoard[c + 2][q + 2] == 0) {

                                if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagonalUp++;
                                } else if (gameBoard[c + 3][q + 3] == opponentNumber) {
                                    diagonalUp = 0;
                                }
                            } else {
                                diagonalUp = 0;
                            }
                        } else {
                            diagonalUp = 0;
                        }
                        if (gameBoard[c - 1][q + 1] == playerNumber) {
                            diagonalDown++;
                            if (gameBoard[c - 2][q + 2] == playerNumber) {
                                diagonalDown++;
                                if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagonalDown++;
                                    myFourInRow = 1;
                                } else if (gameBoard[c - 3][q + 3] == opponentNumber) {
                                    diagonalDown = 0;
                                }
                            } else if (gameBoard[c - 2][q + 2] == 0) {

                                if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagonalDown++;
                                } else if (gameBoard[c - 3][q + 3] == opponentNumber) {
                                    diagonalDown = 0;
                                }
                            } else {
                                diagonalDown = 0;
                            }
                        } else if (gameBoard[c - 1][q + 1] == 0) {

                            if (gameBoard[c - 2][q + 2] == playerNumber) {
                                diagonalDown++;

                                if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagonalDown++;
                                } else if (gameBoard[c - 3][q + 3] == opponentNumber) {
                                    diagonalDown = 0;
                                }
                            } else if (gameBoard[c - 2][q + 2] == 0) {

                                if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagonalDown++;
                                } else if (gameBoard[c - 3][q + 3] == opponentNumber) {
                                    diagonalDown = 0;
                                }
                            } else {
                                diagonalDown = 0;
                            }
                        } else {
                            diagonalDown = 0;
                        }
                    } else if (c < 3) {
                        if (gameBoard[c + 1][q + 1] == playerNumber) {
                            diagonalUp++;
                            if (gameBoard[c + 2][q + 2] == playerNumber) {
                                diagonalUp++;
                                if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagonalUp++;
                                    myFourInRow = 1;
                                } else if (gameBoard[c + 3][q + 3] == opponentNumber) {
                                    diagonalUp = 0;
                                }
                            } else if (gameBoard[c + 2][q + 2] == 0) {

                                if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagonalUp++;
                                } else if (gameBoard[c + 3][q + 3] == opponentNumber) {
                                    diagonalUp = 0;
                                }
                            } else {
                                diagonalUp = 0;
                            }
                        } else if (gameBoard[c + 1][q + 1] == 0) {

                            if (gameBoard[c + 2][q + 2] == playerNumber) {
                                diagonalUp++;

                                if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagonalUp++;
                                } else if (gameBoard[c + 3][q + 3] == opponentNumber) {
                                    diagonalUp = 0;
                                }
                            } else if (gameBoard[c + 2][q + 2] == 0) {

                                if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    diagonalUp++;
                                } else if (gameBoard[c + 3][q + 3] == opponentNumber) {
                                    diagonalUp = 0;
                                }
                            } else {
                                diagonalUp = 0;
                            }
                        } else {
                            diagonalUp = 0;
                        }
                    } else {
                        if (gameBoard[c - 1][q + 1] == playerNumber) {
                            diagonalDown++;
                            if (gameBoard[c - 2][q + 2] == playerNumber) {
                                diagonalDown++;
                                if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagonalDown++;
                                    myFourInRow = 1;
                                } else if (gameBoard[c - 3][q + 3] == opponentNumber) {
                                    diagonalDown = 0;
                                }
                            } else if (gameBoard[c - 2][q + 2] == 0) {
                                if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagonalDown++;
                                } else if (gameBoard[c - 3][q + 3] == opponentNumber) {
                                    diagonalDown = 0;
                                }
                            } else {
                                diagonalDown = 0;
                            }
                        } else if (gameBoard[c - 1][q + 1] == 0) {

                            if (gameBoard[c - 2][q + 2] == playerNumber) {
                                diagonalDown++;

                                if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagonalDown++;
                                } else if (gameBoard[c - 3][q + 3] == opponentNumber) {
                                    diagonalDown = 0;
                                }
                            } else if (gameBoard[c - 2][q + 2] == 0) {

                                if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    diagonalDown++;
                                } else if (gameBoard[c - 3][q + 3] == opponentNumber) {
                                    diagonalDown = 0;
                                }
                            } else {
                                diagonalDown = 0;
                            }
                        } else {
                            diagonalDown = 0;
                        }
                    }
                }
                if (diagonalDown > maxDiagonal) {
                    maxDiagonal = diagonalDown;
                }
                if (diagonalUp > maxDiagonal) {
                    maxDiagonal = diagonalUp;
                }
                diagonalUp = 0;
                diagonalDown = 0;
                if (gameBoard[c][q] == opponentNumber) {
                    if (c == 3) {
                        if (gameBoard[c + 1][q + 1] == opponentNumber) {
                            opponentDiagonal++;
                            if (gameBoard[c + 2][q + 2] == opponentNumber) {
                                opponentDiagonal++;
                                if (gameBoard[c + 3][q + 3] == opponentNumber) {
                                    opponentDiagonal++;
                                    opponentWin = -1;
                                } else if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    opponentDiagonal = 0;
                                }
                            } else if (gameBoard[c][q + 2] == 0) {

                                if (gameBoard[c + 3][q + 3] == opponentNumber) {
                                    opponentDiagonal++;
                                } else if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    opponentDiagonal = 0;
                                }
                            } else {
                                opponentDiagonal = 0;
                            }
                        } else if (gameBoard[c + 1][q + 1] == 0) {

                            if (gameBoard[c + 2][q + 2] == opponentNumber) {
                                opponentDiagonal++;

                                if (gameBoard[c + 3][q + 3] == opponentNumber) {
                                    opponentDiagonal++;
                                } else if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    opponentDiagonal = 0;
                                }
                            } else if (gameBoard[c + 2][q + 2] == 0) {

                                if (gameBoard[c + 3][q + 3] == opponentNumber) {
                                    opponentDiagonal++;
                                } else if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    opponentDiagonal = 0;
                                }
                            } else {
                                opponentDiagonal = 0;
                            }
                        } else {
                            opponentDiagonal = 0;
                        }
                        if (gameBoard[c - 1][q + 1] == opponentNumber) {
                            opponentDiagonalDown++;
                            if (gameBoard[c - 2][q + 2] == opponentNumber) {
                                opponentDiagonalDown++;
                                if (gameBoard[c - 3][q + 3] == opponentNumber) {
                                    opponentDiagonalDown++;
                                    opponentWin = -1;
                                } else if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    opponentDiagonalDown = 0;
                                }
                            } else if (gameBoard[c - 2][q + 2] == 0) {

                                if (gameBoard[c - 3][q + 3] == opponentNumber) {
                                    opponentDiagonalDown++;
                                } else if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    opponentDiagonalDown = 0;
                                }
                            } else {
                                opponentDiagonalDown = 0;
                            }
                        } else if (gameBoard[c - 1][q + 1] == 0) {

                            if (gameBoard[c - 2][q + 2] == opponentNumber) {
                                opponentDiagonalDown++;

                                if (gameBoard[c - 3][q + 3] == opponentNumber) {
                                    opponentDiagonalDown++;
                                } else if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    opponentDiagonalDown = 0;
                                }
                            } else if (gameBoard[c - 2][q + 2] == 0) {

                                if (gameBoard[c - 3][q + 3] == opponentNumber) {
                                    opponentDiagonalDown++;
                                } else if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    opponentDiagonalDown = 0;
                                }
                            } else {
                                opponentDiagonalDown = 0;
                            }
                        } else {
                            opponentDiagonalDown = 0;
                        }
                    } else if (c < 3) {
                        if (gameBoard[c + 1][q + 1] == opponentNumber) {
                            opponentDiagonal++;
                            if (gameBoard[c + 2][q + 2] == opponentNumber) {
                                opponentDiagonal++;
                                if (gameBoard[c + 3][q + 3] == opponentNumber) {
                                    opponentDiagonal++;
                                    opponentWin = -1;
                                } else if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    opponentDiagonal = 0;
                                }
                            } else if (gameBoard[c + 2][q + 2] == 0) {

                                if (gameBoard[c + 3][q + 3] == opponentNumber) {
                                    opponentDiagonal++;
                                } else if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    opponentDiagonal = 0;
                                }
                            } else {
                                opponentDiagonal = 0;
                            }
                        } else if (gameBoard[c + 1][q + 1] == 0) {

                            if (gameBoard[c + 2][q + 2] == opponentNumber) {
                                opponentDiagonal++;

                                if (gameBoard[c + 3][q + 3] == opponentNumber) {
                                    opponentDiagonal++;
                                } else if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    opponentDiagonal = 0;
                                }
                            } else if (gameBoard[c + 2][q + 2] == 0) {

                                if (gameBoard[c + 3][q + 3] == opponentNumber) {
                                    opponentDiagonal++;
                                } else if (gameBoard[c + 3][q + 3] == playerNumber) {
                                    opponentDiagonal = 0;
                                }
                            } else {
                                opponentDiagonal = 0;
                            }
                        } else {
                            opponentDiagonal = 0;
                        }
                    } else {
                        if (gameBoard[c - 1][q + 1] == opponentNumber) {
                            opponentDiagonalDown++;
                            if (gameBoard[c - 2][q + 2] == opponentNumber) {
                                opponentDiagonalDown++;
                                if (gameBoard[c - 3][q + 3] == opponentNumber) {
                                    opponentDiagonalDown++;
                                    myFourInRow = 1;
                                } else if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    opponentDiagonalDown = 0;
                                }
                            } else if (gameBoard[c - 2][q + 2] == 0) {

                                if (gameBoard[c - 3][q + 3] == opponentNumber) {
                                    opponentDiagonalDown++;
                                } else if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    opponentDiagonalDown = 0;
                                }
                            } else {
                                opponentDiagonalDown = 0;
                            }
                        } else if (gameBoard[c - 1][q + 1] == 0) {
                            if (gameBoard[c - 2][q + 2] == opponentNumber) {
                                opponentDiagonalDown++;
                                if (gameBoard[c - 3][q + 3] == opponentNumber) {
                                    opponentDiagonalDown++;
                                } else if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    opponentDiagonalDown = 0;
                                }
                            } else if (gameBoard[c - 2][q + 2] == 0) {
                                if (gameBoard[c - 3][q + 3] == opponentNumber) {
                                    opponentDiagonalDown++;
                                } else if (gameBoard[c - 3][q + 3] == playerNumber) {
                                    opponentDiagonalDown = 0;
                                }
                            } else {
                                opponentDiagonalDown = 0;
                            }
                        } else {
                            opponentDiagonalDown = 0;
                        }
                    }
                }
                if (opponentDiagonalDown > maxDiagonal) {
                    opponentDiagScore = opponentDiagonalDown;
                }
                if (opponentDiagonal > maxDiagonal) {
                    opponentDiagScore = opponentDiagonal;
                }
                opponentDiagonal = 0;
                opponentDiagonalDown = 0;
            }
        }
        int columnScore = 0;
        int maxColumnScore = 0;

        int opponentColumn = 0;
        int opponentMaxColumn = 0;
        for(int t = 0; t < 4; t++) {
            for(int f = 0; f < 7; f++) {
                if(gameBoard[t][f] == playerNumber) {
                    columnScore++;
                    if(gameBoard[t + 1][f] == playerNumber) {
                        columnScore++;
                        if(gameBoard[t + 2][f] == playerNumber) {
                            columnScore++;
                            if(gameBoard[t + 3][f] == playerNumber) {
                                columnScore++;
                                myFourInRow = 1;
                            } else if(gameBoard[t + 3][f] == opponentNumber){
                                columnScore = 0;
                            }
                        } else if(gameBoard[t + 2][f] == opponentNumber){
                            columnScore = 0;
                        }
                    } else if(gameBoard[t + 1][f] == opponentNumber) {
                        columnScore = 0;
                    }
                } else if(gameBoard[t][f] == opponentNumber){
                    columnScore = 0;
                }
                if(columnScore > maxColumnScore) {
                    maxColumnScore = columnScore;
                }
                columnScore = 0;
                if(gameBoard[t][f] == opponentNumber) {
                    opponentColumn++;
                    if(gameBoard[t + 1][f] == opponentNumber) {
                        opponentColumn++;
                        if(gameBoard[t + 2][f] == opponentNumber) {
                            opponentColumn++;
                            if(gameBoard[t + 3][f] == opponentNumber) {
                                opponentColumn++;
                                opponentWin = -1;
                            } else if(gameBoard[t + 3][f] == playerNumber){
                                opponentColumn = 0;
                            }
                        } else if(gameBoard[t + 2][f] == playerNumber){
                            opponentColumn = 0;
                        }
                    } else if(gameBoard[t + 1][f] == playerNumber) {
                        opponentColumn = 0;
                    }
                } else if(gameBoard[t][f] == playerNumber){
                    opponentColumn = 0;
                }
                if(opponentColumn > opponentMaxColumn) {
                    opponentMaxColumn = opponentColumn;
                }
                opponentColumn = 0;
            }
        }

        double weightedValue = 0.0;
        if(myFourInRow == 1 && opponentWin == 0) {
            weightedValue = 1.0;
        } else if(opponentWin == -1 && myFourInRow == 0) {
            weightedValue = -1.0;
        } else if(opponentWin == -1 && myFourInRow == 1) {
            weightedValue = -1;
        } else {
            weightedValue = ((maxColumnScore * maxColumnScore + maxRowScore * maxRowScore + maxDiagonal * maxDiagonal) -
             (opponentMaxColumn * opponentMaxColumn + opponentMaxRow * opponentMaxRow + opponentDiagScore * opponentDiagScore))
              / 100.0;
        }
        return weightedValue;
    }

    private Move getBestMove(ArrayList<Move> moves) {

        double max = -1.0;
        Move bestMove = moves.get(0);

        for (Move cm : moves) {
            if (cm.value > max) {
                max = cm.value;
                bestMove = cm;
            }
        }

        return bestMove;
    }



}
